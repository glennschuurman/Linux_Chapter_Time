Linux ChapterTime
==========================

## Directory Structure

```bash
/
├── bin         | Essential Commands
├── boot        | Boot files, for example bios bootloader
├── dev         | Devices, for example harddisks and network adapters
├── etc         | Host specific configuration, for example docker config
├── home        | Contains user home directories
|   └── User    | The user home, also reffered to by ~
├── initrd.img  | The Initial Ramdisk, This creates a temporary root to allow booting other processes
├── lib         | x86 lib files, most commonly .so files
├── lib64       | x64 lib files
├── media       | Contains mount points for replaceable medi
├── mnt         | Mount point for mounting a file system temporarily
├── opt         | Add-on application software packages
├── proc        | Virtual directory for system information
├── root        | Home directory for the root user
├── run         | Run-time variable data, for example pid files
├── sbin        | Essential system binaries
├── srv         | Data for services provided by the system
├── swapfile    | A file where RAM is stored in case of a shortage
├── sys         | Virtual directory for system information
├── tmp         | A temporary directory, all files are removed on shutdown or reboot
├── usr         | Secondary hierarchy
├── var         | Variable Data, for example website data
└── vmlinuz     | The core kernel that allows starting the OS

```

## Some Usefull Commands
    The Information below is derived from the Linux Man Pages
    For reference please visit https://ss64.com/bash/
### **man**
    Show the manual of a command

### **ls**
    Lists files in a directory
#### Parameters:
    -a List all including hidde
    -l Prints output in lines
    -Z Show SeLinux context
    -d only list directory
    -s show size
#### Example
`ls -alZ`

### **wc**
    Count the words of an output
#### Parameters:
    -l Change wordcount to Linecount
#### Example
`cat /etc/passwd | wc -l`


### **tar**
    Pack and unpack an archive
#### Parameters
    -x Extract archive
    -v Use verbose mode
    -f Use a file as input
    -z Extract a gzip
    -t List the files in the archive
    -c Compress the input to an archive

#### Example
`tar -xfz archive.tar.gz` Extract the archive to the current directory

### **gunzip**
    Unpack a gz file
    removes archive by default
#### Parameters
    -k Keep file
#### Example
`gunzip -k archive.gz` Extract the archive to the current directory and keeps the archive

### **rm**
    Remove a file or directory
#### Parameters
    -r Recursive
    -f force
    -i prompt (interactive)
    -d remove empty directrory
    --no-preserve-root Never use this unless you know what you are doing!
        This does not treat '/' specially
#### Example
`rm -r /tmp/` Remove all content in /tmp/

### **cp**
    Copy a file or directory {Source} {Targer}
#### Parameters
    -r Recursive
    -f force
#### Example
`cp /tmp/script ~/Desktop/` Copies script to Desktop

### **mv**
    Move a file or directory {Source} {Targer}
#### Parameters
    -r Recursive
    -f force
#### Example
`mv /tmp/script ~/Desktop/` Moves script to Desktop

### **echo**
    Prints the parameter to stdout
#### Parameters
    -n no newline
    -e enable interpretation of backslash escapes
    -E disable interpretation of backslash escapes
#### Example
    echo $Path

### **cat** 
    Concatenate files and print on the standard output
#### Parameters
    -A Show all
    -b number nonempty output lines, overrides -n
    -n number all output lines
#### Example
`cat -n /tmp/file`

### **tac**
    Concatenate and print files in reverse
#### Parameters
    -A Show all
    -b number nonempty output lines, overrides -n
    -n number all output lines
#### Example
`cat -n /tmp/file`

### **grep**
    filters output according to a regexp
#### Parameters

#### Example
`cat /etc/passwd | grep $USER`

### **|** (pipe)
    The pipe is used to pipe output of a command into another command
#### Example
`ls -l | wc -l` displays the numer of lines outputted by ls

### **&&**
    Concatinate multipe commands
#### Example
`sudo apt update && apt install openjdk8-jre`

### **less**
    Less opens a file read-only in a vim like enviroment for better viewing
#### Example
`less /etc/passwd`

### **vi**(m)
    Vim is a Text editor with pleny of macro's
    Exiting vim is done by typing :q!
#### Example
`vi /etc/passwd`

### **nano**
    Nano is a more graphical commandline editor. 
#### Example 
`nano /etc/passwd`

### **tail**
    Tail can be used to see the last lines of a file or track the output of a changing file
#### Parameters
    -f Follows the output of the file
    -{Number X} Prints X lines from the end of the file
#### Example
`tail -f /var/log/syslog`

### **ln**
    ln is used to create a link in the filesystem, this can be seen as s shotcut in Windows but on a lower level.
#### Parameters
    -d, -F, --directory
       Allow the superuser to attempt to hard link directories (note: will probably fail due to system restrictions, even for the superuser) 
    -f, --force
       Remove existing destination files 
    -i, --interactive
        Prompt whether to remove destinations 
    -L, --logical
        Make hard links to symbolic link references 
    -n, --no-dereference
        Treat destination that is a symlink to a direc tory as if it were a normal file 
    -P, --physical
        Make hard links directly to symbolic links 
    -s, --symbolic
        Make symbolic links instead of hard links 
    -S, --suffix=SUFFIX
        Override the usual backup suffix 
    -t, --target-directory=DIRECTORY
        Specify the DIRECTORY in which to create the links 
    -T, --no-target-directory
        Treat NewLinkFile as a normal file 
    -v, --verbose
        Print name of each linked file 
#### Example
`ln -s /opt/bin/test /usr/local/bin/test` Creates a symlink from /opt/bin/test to /usr/local/bin

### **htop**
Show the currently running processes interactively   

### **top**
Show the currently running processes interactively

### **touch**
    Creates a file
#### Example
`touch hello` Creates a file called hello in the current directory

### **tree**
    Shows an output of the directory structure

### **tee**
    Redirect output to multiple files, copies standard input to standard output and also to any files given as arguments.
#### Parameters
    -a Append standard input to the given files rather than overwriting them.
    -i Ignore interrupt signals.
#### Example
`ps -ax | tee processes.txt | more`

### **screen**
Used to connect with a serial device, for example a network switch or a arduino

### **nohup**
Do not hangup the process if the terminal is closed. (handy for long running jobs)

### **sudo**
    sudo allows a permitted user to execute a command as the superuser or another user, as specified in the sudoers file.
#### Parameters
    -s Use shell as specified in the SHELL Variable
    -i Simulate Login
    -k Kill the sudo authentication and require a password the next time sudo is run
    -u Run command as specified user
#### Example
`sudo -su ansible` change to user ansible
### **su**
    Switch User
#### Parameters
    -
    -c COMMAND 
        Pass COMMAND, a single command line to run, to the shell with a -c option instead of starting an interactive shell.
    -l Make the shell a login shell. 
    -m , -p  Do not change the environment variables.

#### Example
`su - ansible `

### **ps**
    Process status, information about processes running in memory.
#### Parameters
    See: https://ss64.com/bash/ps.html

### **whoami**
    Prints the current user

### **lsusb**
    Lists all connected USB Devices.

### **``**
    Using `` allows you to execute a commmand within another, this allows you to fetch output and use it in the other command
#### Example
```bash 
echo `whoami`
```

### **dmesg**
    Prints the Kernel Mesages (handy for debugging kernel issues)

<!-- ### forground/ background ctrl-Z jobs disown  -->
## Exercise 1 (permissions)
### File permissions
Create a file using the commandline with the following permissions:
|       | R | W | X |
|-------|---|---|---|
| Owner | X | X | X |
| Group |   |   | X |
| World |   |   | X |

### File Ownership
Change the file owner to your user 
Change the owner group to root


### Recursive Permissions and Ownership
Change the permissions of the noaccess folder and all files to the following 
|       | R | W | X |
|-------|---|---|---|
| Owner | X | X |   |
| Group | X | X |   |
| World |   |   |   | 

Now change the Owner of the noaccess folder and all files to your own user and own group

### Folder Permissions (Sticky bit)
Create a new folder that applies it's permission rules to every new file in the folder

## Exercise 2 (file extentions)
Make sure you can run the `user` file in the `exercise2` folder using the following command\
``` shell
./user
```

## Exercise 3 (Variables)
### Printing the enviroment (on terminal)
print all your enviromental variables in your system

### Printing an indivudial variable
print the following variables:
``` shell
HTTP_PROXY
PWD
PATH
```

### Extend your path (temporarily) to also use the Exercise2 folder
Make sure you can now execute user without the ./\
Extending your path permanently is commonly done in `/etc/enviroment` or (depending on your interpreter) `~/.bashrc`

## Exercise 4 (logging)
### Check for errors in the kernel mesages
### Check syslog output


## Exercise 5 (Filesystems)
**!!! Continue with CAUTION !!!**
### List all your disks on your machine using fdisk

### list your free space using df

### Mount the mountMe image on mountHere 

Now unmount the image using umount

## Useradd
Useradd can be used to add a user to your system, this command is interactive. A userhome and profile is created for the new user. 

## /etc/passwd
/etc/passwd lists all users on the system, even the ones without shell or credentials

## Archive
There are several ways to deal with archives on linux.
The following extentions are common:  
### tar
tar is the most common archive within linux
#### Compress
`tar -cvf file.tar /path/to/directory-or-file`
#### Uncompress
`tar -xvf file.tar`

### tar.gz
Uses tar and gzip compression to get a better compression ratio
#### Compress
`tar -czvf file.tar.gz /path/to/directory-or-file`
#### Uncompress
`tar -xzvf file.tar.gz`

### tar.bz2
Uses tar and bzip2 compression, bzip compresses better than gzip but uses more memory and cpu.
#### Compress
`tar -cjvf file.tar.bz2 /path/to/directory-or-file`

#### Uncompress
`tar -xjvf file.tar.bz2`

### zip
Zip is one of the most used archives accros the world, even having native support in windows.
#### Compress
`zip file.zip /path/to/directory-or-file`\
`pack file.zip /path/to/directory-or-file`
#### Uncompress
`unzip file.zip`\
`unpack file.zip`

### gzip

#### Compress
`gzip /path/to/directory-or-file`
#### Uncompress
`gzip -d file.gz`\
`gunzip file.gz`

### rar

#### Compress
`rar a /path/to/directory-or-file`
#### Uncompress
`unrar file.rar`

## dd
DataDump, Can be used to copy an entire disk or create an empty image file

## apt
The default debian / ubuntu package manager

## dpkg
dpkg can be used to install .deb files from the commandline.\
this is done by the following command `sudo dpkg -i file.deb`

## snap
Snap is a packagemanager for GUI processes used by ubuntu.

## yum
Yum is the redhat, centOs and fedora packagemanager. 

# Just ask questions